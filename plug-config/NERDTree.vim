"  NERDTree keybindings
" ----------------------

" toggle tree
nnoremap <Leader>f :NERDTreeToggle<CR>  
" toggle tree and find open file
nnoremap <silent> <Leader>v :NERDTreeFind<CR>




"  NERDTree settings
" -------------------

let NERDTreeMinimalUI=1
let NERDTreeDirArrows = 0
let NERDTreeQuitOnOpen = 1
let NERDTreeAutoDeleteBuffer = 1
