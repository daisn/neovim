    if has('termguicolors')
      set termguicolors
    endif
    set background=dark

    let g:gruvbox_material_background = 'hard'
    let g:gruvbox_material_enable_italic = 1
    let g:gruvbox_material_disable_italic_comment = 0
    let g:gruvbox_material_enable_bold = 1
    let g:gruvbox_material_ui_contrast = 'low' " high
    let g:gruvbox_material_visual = 'red background'
    let g:gruvbox_material_menu_selection_background = 'grey'
    let g:gruvbox_material_sign_column_background = 'none'
    let g:gruvbox_material_better_performance = 1

    colorscheme gruvbox-material
   " let g:lightline.colorscheme = 'gruvbox_material'
