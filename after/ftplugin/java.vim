compiler javag
noremap <buffer><leader>8 :w<CR>:make<CR>:cwindow<CR>
noremap <buffer><leader>-  :!java -cp *.jar %<CR>
noremap <buffer><leader>9 :!clear;echo <C-r>=expand('%:r')<CR> \| xargs java<CR>
noremap <buffer><F10> :cprevious<CR>
noremap <buffer><F11> :cnext<CR>
noremap <buffer><F12> :cclose<CR>
